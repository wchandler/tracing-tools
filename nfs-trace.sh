#!/usr/bin/env bash

# USAGE:
# bash nfs-trace.sh [PIDS]
# PIDS: A comma separated list of pids to trace

if [[ "$EUID" -ne 0 ]]; then
  echo "$0 requires root access to access debugfs"
  exit 1
fi

trap catch_sigint SIGINT

pid_args="$1"; shift;
actually_run="$1"; shift;

declare -a kprobes
declare -a kprobe_names
declare -a nfs_events
declare -a rpc_events

kprobes=( 'p:nfs_probe_crazy nfs4_file_open name=+0(+40(+24(%si))):string' 'r:nfs_probe_crazy_ret nfs4_file_open ret=$retval:s32' )
kprobe_names=( "nfs_probe_crazy" "nfs_probe_crazy_ret" )
nfs_events=( "nfs4:nfs4_access" "nfs4:nfs4_getattr" "nfs4:nfs4_mkdir" "nfs4:nfs4_open_file" "nfs4:nfs4_read" "nfs4:nfs4_readdir" "nfs4:nfs4_readlink" "nfs4:nfs4_remove" "nfs4:nfs4_rename" "nfs4:nfs4_write" )
rpc_events=( "sunrpc:rpc_task_begin" "sunrpc:rpc_task_complete" "sunrpc:rpc_task_sleep" "sunrpc:rpc_task_wakeup" "sunrpc:xprt_transmit" "sunrpc:xprt_complete_rqst" )
pid_filter=""
out_file=/tmp/test_trace

function main()
{
  format_pids
  
  if [[ "$actually_run" = "true" ]]; then
    if [[ ! -z "$pid_filter" ]]; then
      echo "writing pids to /sys/kernel/debug/tracing/set_event_pid"
      
      echo "$pid_filter" > /sys/kernel/debug/tracing/set_event_pid
      echo "$pid_filter" > /sys/kernel/debug/tracing/set_ftrace_pid
      if [[ $? -ne 0 ]]; then
        die "failed to write to set_event_pid"
      fi
    fi

    echo "Writing trace to $out_file. Press Ctrl+C to exit"
    open_free_buffer

    set_kprobes
    #set_tracepoints

    cat /sys/kernel/debug/tracing/trace_pipe > $out_file

    cleanup
  fi
}

function format_pids()
{
  if [[ ! -z "$pid_args" ]]; then
    for pid in $( echo $pid_args | tr ',' ' ' ); do
      for tid_path in /proc/$pid/task/*; do
        tid=${tid_path##*/}
        pid_filter="$pid_filter $tid"
      done
    done
  fi
}

function set_kprobes()
{
  for kprobe in "${kprobes[@]}"; do
    if ! echo "$kprobe" >> /sys/kernel/debug/tracing/kprobe_events; then
      die "failed to create kprobe $kprobe"
    fi
  done

  for kprobe_name in "${kprobe_names[@]}"; do
    if ! echo "$kprobe_name" >> /sys/kernel/debug/tracing/set_event; then
      die "failed to enable kprobe $kprobe_name"
    fi
  done
}

function set_tracepoints()
{
  for nfs_tp in "${nfs_events[@]}"; do
    if ! echo "$nfs_tp" >> /sys/kernel/debug/tracing/set_event; then
      die "failed to enable tracepoint $nfs_tp"
    fi
  done

  for rpc_tp in "${rpc_events[@]}"; do
    if ! echo "$rpc_tp" >> /sys/kernel/debug/tracing/set_event; then
      die "failed to enable tracepoint $rpc_tp"
    fi
  done
}

# We open free_buffer at the start of tracing, when the
# script exits the fd is closed and the kernel ring
# buffer is freed
function open_free_buffer()
{
  exec {free_fd}>/sys/kernel/debug/tracing/free_buffer
}

function cleanup()
{
  echo > /sys/kernel/debug/tracing/set_event
  echo > /sys/kernel/debug/tracing/kprobe_events

  if [[ ! -z "$pid_filter" ]]; then
    echo > /sys/kernel/debug/tracing/set_event_pid
  fi
}

function die()
{
  echo "ERROR! $1"
  cleanup
  exit 1
}

function catch_sigint()
{
  echo "Ending trace"
  cleanup
}

    
main "$@"

